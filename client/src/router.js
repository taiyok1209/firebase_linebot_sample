import Vue from 'vue'
import Router from 'vue-router'
import List from './components/List.vue'
import Rank from './components/Rank.vue'
import Gallery from './components/Gallery.vue'
import Show from './components/Show.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'gallery',
      component: Gallery
    },
    {
      path: '/list',
      name: 'list',
      component: List
    },
    {
      path: '/rank',
      name: 'rank',
      component: Rank
    },
    {
      path: '/show',
      name: 'show',
      component: Show
    },
  ]
})
