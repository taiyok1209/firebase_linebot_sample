/**
 * テキストメッセージを処理するクラス
 *
 * @class TextMessageProcessor
 */
class TextMessageProcessor {

  /**
   *Creates an instance of ImageProcessor.
   * @param {*} admin
   * @param {*} client
   * @memberof ImageProcessor
   */
  constructor(admin, client) {
    this.firebaseAdmin = admin;
    this.lineClient = client;
  }

  /**
   * メッセージをDBに保存して同じ文言を返答する
   *
   * @param {*} event
   * @returns
   * @memberof TextMessageProcessor
   */
  async saveAndReplyTextMessage(event) {
    await this.saveTextMessage(event);
    return this.replyTextMessage(event);
  }

  /**
   * テキストメッセージを保存する
   *
   * @param {*} event
   * @returns
   * @memberof TextMessageProcessor
   */
  saveTextMessage(event) {
    const info = {
      source: event.source,
      timestamp: event.timestamp,
      message: event.message,
    }
    return this.firebaseAdmin.database().ref('/texts').push(info);
  }

  /**
   * 送られてきたメッセージと同じ文面を返答する
   *
   * @param {*} event
   * @returns
   * @memberof TextMessageProcessor
   */
  replyTextMessage(event) {
    // create a echoing text message
    const echo = {
      type: 'text',
      text: event.message.text
    };

    // use reply API
    return this.lineClient.replyMessage(event.replyToken, echo);
  }
}

module.exports = TextMessageProcessor;