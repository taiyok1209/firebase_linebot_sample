const dl = require('datalib');

class Aggregater {

  constructor(admin, client) {
    this.firebaseAdmin = admin;
    this.lineClient = client;
  }

  /**
   * 投稿情報の一覧を取得する。
   *
   * @returns
   * @memberof Aggregater
   */
  async getUploadedList() {
    const snapshot = await this.firebaseAdmin.database().ref('/messages').orderByChild('timestamp').once('value');
    var val = snapshot.val();
    var data = Object.keys(val).map((key) => {
      return val[key];
    });
    const uploadedList = this.appendLineUserInfo(data, d => d.source.userId);
    return uploadedList;
  }

  /**
   * 投稿のランキングを取得する。
   *
   * @returns
   * @memberof Aggregater
   */
  async getUploadRank() {
    const snapshot = await this.firebaseAdmin.database().ref('/messages').once('value');
    const rank = this.calcRank(snapshot);
    const userRank = this.appendLineUserInfo(rank, d => d['source.userId']);
    return userRank
  }

  /**
   * ユーザIDをキーにしてカウント集計し、ランキングを計算する。
   *
   * @param {*} snapshot
   * @returns
   * @memberof Aggregater
   */
  calcRank(snapshot) {
    const val = snapshot.val();
    const data = Object.keys(val).map((key) => {
      return val[key];
    });

    const dlData = dl.read(data);
    const count = dl.groupby('source.userId').count().execute(dlData).sort(dl.comparator('-count'));
    const rank = count.map((data, index) => Object.assign({
      'rank': index + 1
    }, data));

    return rank;
  }

  /**
   * ユーザIDからLineのプロファイル情報を取得し付与する。
   *
   * @param {*} data
   * @param {*} getUserIdFunc
   * @returns
   * @memberof Aggregater
   */
  async appendLineUserInfo(data, getUserIdFunc) {
    console.log(data);
    const userAppended = await Promise.all(data.map(async (d) => {
      try {
        const userId = getUserIdFunc(d);
        const profile = await this.lineClient.getProfile(userId);
        const newData = Object.assign({
          'profile': profile
        }, d);
        delete newData['source.userId'];
        return newData;
      } catch (err) {
        return Promise.reject(err);
      }
    }));
    return userAppended;
  }
}

module.exports = Aggregater;