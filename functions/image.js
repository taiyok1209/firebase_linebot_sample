const path = require('path');
const os = require('os');
const fs = require('fs');

/**
 * 画像の投稿を処理するクラス
 *
 * @class ImageProcessor
 */
class ImageProcessor {

  /**
   *Creates an instance of ImageProcessor.
   * @param {*} admin
   * @param {*} client
   * @param {*} cloudinary
   * @memberof ImageProcessor
   */
  constructor(admin, client, cloudinary) {
    this.firebaseAdmin = admin;
    this.lineClient = client;
    this.cloudinary = cloudinary;
  }

  /**
   * 画像を保存し、投稿者の情報を保存してメッセージを返す
   *
   * @param {*} event
   * @returns
   * @memberof ImageProcessor
   */
  async saveImageContent(event) {
    const imagePath = await this.getLineImageContent(event.message.id);
    const [file, metadata] = await this.uploadImageContent(imagePath);
    await this.saveMessageInfo(event, file.name);
    return this.lineClient.replyMessage(event.replyToken, {
      type: 'text',
      text: '画像をアップロードしました'
    })
  }

  /**
   * Lineに投稿されたコンテンツを取得する
   *
   * @param {*} messageId
   * @returns
   * @memberof ImageProcessor
   */
  async getLineImageContent(messageId) {
    const ext = '.jpg';
    const tmpPath = path.join(os.tmpdir(), messageId + ext);
    const stream = await this.lineClient.getMessageContent(messageId)
    return new Promise((resolve, reject) => {
      const writable = fs.createWriteStream(tmpPath);
      stream.pipe(writable);
      stream.on('end', () => {
        resolve(tmpPath);
      });
      stream.on('error', reject);
    });
  }

  /**
   * StorageとCloudinaryにファイルを保存する。
   *
   * @param {*} imagePath
   * @returns
   * @memberof ImageProcessor
   */
  async uploadImageContent(imagePath) {
    const cloudinaryInfo = await this.uploadImageToCloudinary(imagePath);
    await this.firebaseAdmin.database().ref('/cloudinary').push(cloudinaryInfo)
    return this.firebaseAdmin.storage().bucket().upload(imagePath);
  }

  /**
   * Cloudinaryにファイルをアップロードする
   *
   * @param {*} imagePath
   * @returns
   * @memberof ImageProcessor
   */
  uploadImageToCloudinary(imagePath) {
    return new Promise((resolve, reject) => {
      this.cloudinary.v2.uploader.upload(imagePath, (err, result) => {
        if (err) return reject(err);
        return resolve(result);
      });
    });
  }

  /**
   * メッセージの情報（投稿者、時間、画像）を保存する。
   *
   * @param {*} info
   * @param {*} image
   * @returns
   * @memberof ImageProcessor
   */
  saveMessageInfo(info, image) {
    var savedInfo = {
      source: info.source,
      timestamp: info.timestamp,
      image: image,
    }
    return this.firebaseAdmin.database().ref('/messages').push(savedInfo);
  }
}

module.exports = ImageProcessor;