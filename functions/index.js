const functions = require('firebase-functions');
const line = require('@line/bot-sdk');
const cloudinary = require('cloudinary')
const cors = require('cors')({
  origin: true
});

// initialize Firebase Admin SDK
const admin = require('firebase-admin');
admin.initializeApp();

// create LINE SDK config from env variables
const config = {
  channelAccessToken: functions.config().line.channel_access_token,
  channelSecret: functions.config().line.channel_secret,
};

// create LINE SDK client
const client = new line.Client(config);

// cloudinary
cloudinary.config({
  cloud_name: functions.config().cloudinary.cloud_name,
  api_key: functions.config().cloudinary.api_key,
  api_secret: functions.config().cloudinary.api_secret
});

// 処理クラス
const TextMessageProcessor = require('./text');
const textMessageProcessor = new TextMessageProcessor(admin, client);
const ImageProcessor = require('./image');
const imageProcessor = new ImageProcessor(admin, client, cloudinary);
const Aggregater = require('./aggregater');
const aggregater = new Aggregater(admin, client);

// LINEの応答を生成
exports.line = functions
  .region('asia-northeast1')
  .https.onRequest(async (request, response) => {
    try {
      const result = await Promise.all(request.body.events.map(handleEvent));
      response.json(result);
    } catch (err) {
      console.error(err);
      response.status(500).end();
    }
  });

// 投稿の一覧を返す
exports.list = functions
  .region('asia-northeast1')
  .https.onRequest((request, response) => {
    cors(request, response, async () => {
      try {
        const result = await aggregater.getUploadedList();
        response.json(result);
      } catch (err) {
        console.error(err);
        response.status(500).end();
      }
    })
  });

// 投稿ランキングを返す
exports.rank = functions
  .region('asia-northeast1')
  .https.onRequest((request, response) => {
    cors(request, response, async () => {
      try {
        const result = await aggregater.getUploadRank();
        response.json(result);
      } catch (err) {
        console.error(err);
        response.status(500).end();
      }
    })
  });

/**
 * LINEからのメッセージを処理する
 *
 * @param {*} event
 * @returns
 */
function handleEvent(event) {
  console.log(JSON.stringify(event));
  if (event.type !== 'message') {
    // ignore non-text-message event
    return Promise.resolve(null);
  }

  const messageType = event.message.type;
  if (messageType === 'text') {
    return textMessageProcessor.saveAndReplyTextMessage(event);
  } else if (messageType === 'image') {
    return imageProcessor.saveImageContent(event);
  } else {
    return Promise.resolve(null);
  }
}